import React, { Component } from "react";

import "./WikiSearch.scss";
import Loader from "../Loader/Loader";
import Card from "../Card/Card";

import logo from "./wikipedia_logo.png";

class WikiSearch extends Component {
  constructor(props) {
    super(props);
    this.state = { search: [], query: null };
  }

  componentDidUpdate(prevProps, prevState) {
    const { lang } = this.props;
    const { query } = this.state;
    if (lang !== prevProps.lang && query && query !== "") {
      this.searchWiki();
    }
  }

  searchWiki() {
    this.setState({ search: [], loading: true }, async () => {
      const { query } = this.state;
      const { lang } = this.props;
      const url = `https://${lang}.wikipedia.org/w/api.php?action=query&exintro=1&prop=extracts|pageimages&pithumbsize=250&format=json&redirect=&origin=*&generator=search&gsrsearch=${query}`;

      const res = await fetch(url, { cache: "no-cache" });
      if (res.ok) {
        const data = await res.json();
        const { query, error } = data;
        if (!error && query) {
          const search = Object.keys(data.query.pages || {}).map((k) => ({
            ...data.query.pages[k],
          }));
          this.setState({ search, error: false, loading: false });
        } else {
          this.setState({ search: [], error: true, loading: false });
        }
      } else {
        alert("API Error");
        this.setState({ search: [], error: true, loading: false });
      }
    });
  }

  createExtract(text = "") {
    return text.replace(/<[^>]*>|&quot;/g, "");
  }

  renderThumb(thumb) {
    if (thumb) {
      return thumb.source;
    }
    return logo;
  }

  isExpandable(text = "") {
    return text.replace(/<[^>]*>|&quot;/g, "").length > 300;
  }

  render() {
    const { search, query, loading } = this.state;
    const { lang } = this.props;
    return (
      <div className="wiki-search-comp">
        <div className="form">
          <img src={logo} alt="Wikipedia" />
          <h1>WikiSearch</h1>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              this.searchWiki();
            }}
          >
            <input
              type="text"
              onChange={(e) => this.setState({ query: e.currentTarget.value })}
            />
            <button
              className="button-primary"
              disabled={!query || query === "" || loading}
              type="submit"
            >
              Search
            </button>
          </form>
        </div>
        {search.map((s, i) => (
          <Card
            key={i}
            title={s.title}
            body={this.createExtract(s.extract)}
            image={this.renderThumb(s.thumbnail)}
            link={`https://${lang}.wikipedia.org/?curid=${s.pageid}`}
            expandable={this.isExpandable(s.extract)}
          />
        ))}
        {loading && search.length === 0 && (
          <>
            <Loader />
          </>
        )}
      </div>
    );
  }
}

export default WikiSearch;
