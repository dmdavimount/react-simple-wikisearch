import React, { useState } from "react";

import "./Card.scss";

export default ({ title, body, link, image, expandable = false }) => {
  const [expanded, setExpanded] = useState(false);

  return (
    title && (
      <div className="card-comp">
        <div className="card-header">
          <span className="card-thumb">
            <img src={image} />
          </span>
          <span className="card-title">
            {link ? (
              <a href={link} target="_Blank" rel="noopener noreferrer">
                {title}
              </a>
            ) : (
              title
            )}
          </span>
        </div>
        {body && (
          <p className={`card-body ${expanded ? "expanded" : "shortened"}`}>
            {body}
          </p>
        )}
        {expandable && (
          <div
            className={`card-expand ${expanded ? "opened" : "closed"}`}
            onClick={() => setExpanded(!expanded)}
          >
            Show {expanded ? "less" : "more"}
          </div>
        )}
      </div>
    )
  );
};
