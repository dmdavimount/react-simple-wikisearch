import React, { useState, useEffect } from "react";
import WikiSearch from "./components/WikiSearch/WikiSearch";

import "./App.scss";

const App = () => {
  const langs = ["en", "it", "fr", "de", "es"];
  const [lang, setLang] = useState(langs[0]);
  useEffect(() => {
    const lsLang = window.localStorage.getItem("lang");
    if (langs.indexOf(lsLang) >= 0) setLang(lsLang);
  }, []);
  useEffect(() => {
    window.localStorage.setItem("lang", lang);
  }, [lang]);
  return (
    <div className="cont">
      <div className="header">
        <label htmlFor="lang-sel" className="label-inline">
          Search Wikipedia in:
        </label>
        <select
          id="lang-sel"
          className="lang-sel"
          value={lang}
          onChange={(e) => setLang(e.currentTarget.value)}
        >
          {langs.map((lang, i) => (
            <option key={i} value={lang}>
              {lang}
            </option>
          ))}
        </select>
      </div>
      <div>
        <WikiSearch lang={lang} />
      </div>
      <div className="footer">
        <small>©2020, Davide Mantovani</small>
      </div>
    </div>
  );
};

export default App;
